package be.veeteedev.poc.sse.notification.service;

import be.veeteedev.poc.sse.notification.model.Notification;
import be.veeteedev.poc.sse.notification.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@Service
public class NotificationService {

    @Autowired
    private NotificationRepository notificationRepository;


    public ArrayList<Notification> getAll() {
        return this.notificationRepository.findAll();
    }

    // public Notification get(UUID id) throws EntityNotFoundException {
    public Notification get(Long id) throws EntityNotFoundException {
        Optional<Notification> notification = this.notificationRepository.findById(id);

        if (notification.isPresent()) {
            return notification.get();
        } else {
            throw new EntityNotFoundException();
        }
    }



}
