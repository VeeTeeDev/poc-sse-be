package be.veeteedev.poc.sse.notification.controller;

import be.veeteedev.poc.sse.notification.model.Notification;
import be.veeteedev.poc.sse.notification.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

import javax.persistence.EntityNotFoundException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


@RestController
@RequestMapping("/notification")
public class NotificationController {

    private final List<SseEmitter> emitters = new ArrayList<>();

    @Autowired
    private NotificationService notificationService;


    @GetMapping(value = "/list", produces = {"application/json"})
    public ResponseEntity<Iterable<Notification>> getAll() {
        ArrayList<Notification> notificationList = this.notificationService.getAll();
        return new ResponseEntity<>(notificationList, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = {"application/json"})
    // public ResponseEntity<Notification> get(@PathVariable("id") UUID id) throws EntityNotFoundException {
    public ResponseEntity<Notification> get(@PathVariable("id") Long id) throws EntityNotFoundException {
        Notification notification = this.notificationService.get(id);
        return new ResponseEntity<>(notification, HttpStatus.OK);
    }

    @GetMapping(produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public SseEmitter events() {
        SseEmitter emitter = new SseEmitter();
        emitters.add(emitter);
        emitter.onCompletion(() -> {
            System.out.println("completion for " + emitter.hashCode() );
            emitters.remove(emitter);
        });
        emitter.onError(throwable -> {
            System.out.println("error for " + emitter.hashCode() );
            emitters.remove(emitter);
        });
        emitter.onTimeout(() -> {
            System.out.println("timeout for " + emitter.hashCode() );
            emitters.remove(emitter);
        });
        return emitter;
    }

    @Scheduled(fixedDelay = 2000)
    public void receiveNotification() {
        this.handleNotification(this.notificationService.get((long) (Math.random() * (100 - 1)) + 1));
    }

    private void handleNotification(Notification notification) {
        emitters.parallelStream().forEach(emitter -> {
            try {
                emitter.send(notification);
            } catch (IOException e) {
                emitter.complete();
                emitters.remove(emitter);
            }
        });
    }
}
