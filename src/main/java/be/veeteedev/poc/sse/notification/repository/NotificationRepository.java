package be.veeteedev.poc.sse.notification.repository;

import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

import be.veeteedev.poc.sse.notification.model.Notification;
import org.springframework.data.repository.CrudRepository;

public interface NotificationRepository extends CrudRepository<Notification, Long> {
    ArrayList<Notification> findAll();
    Optional<Notification> findById(Long id);
    //Optional<Notification> findById(UUID id);
}
